json.extract! party_company_detail, :id, :common_name, :official_name, :taxes_registry_number, :country_code, :address, :category_id, :created_at, :updated_at
json.url party_company_detail_url(party_company_detail, format: :json)
