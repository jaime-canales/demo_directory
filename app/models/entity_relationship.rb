class EntityRelationship < ApplicationRecord
  belongs_to :relationship, polymorphic: true
end
