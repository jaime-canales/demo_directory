class Relationship < EntityRelationship
  belongs_to :from, foreign_key: :parent_id, class_name: "Party"
  belongs_to :to, foreign_key: :child_id, class_name: "Party"
end
