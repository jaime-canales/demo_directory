class PartyPersonDetail < ApplicationRecord
  has_one :party, -> { where(detail_type: "PartyPersonDetail") }, foreign_key: :detail_id

  def full_name = "#{first_name} #{last_name}"
  def type = party&.party_type&.name
  def person_types = party&.to_relationships.map{|x| [x&.relationship&.name, x&.from&.detail&.official_name]}
end
