class Accountability < EntityRelationship
  belongs_to :parent, foreign_key: :parent_id, class_name: "Party"
  belongs_to :child, foreign_key: :child_id, class_name: "Party"
end
