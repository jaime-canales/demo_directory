class AccountabilityType < EntityType
  has_many :accountabilities, -> { where(type: "Accountability") }, class_name: "EntityRelationship", foreign_key: :relationship_id
end
