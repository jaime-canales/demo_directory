class PartyGenericDetail < ApplicationRecord
  has_one :party, -> { where(detail_type: "PartyGenericDetail") }, foreign_key: :detail_id
end
