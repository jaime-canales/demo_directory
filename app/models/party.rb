class Party < ApplicationRecord
  belongs_to :party_type
  belongs_to :detail, polymorphic: true
  has_many :from_relationships, foreign_key: :parent_id, class_name: "Relationship"
  has_many :to_relationships, foreign_key: :child_id, class_name: "Relationship"
end
