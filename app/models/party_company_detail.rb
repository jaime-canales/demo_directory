class PartyCompanyDetail < ApplicationRecord
  has_one :party, -> { where(detail_type: "PartyCompanyDetail") }, foreign_key: :detail_id
  belongs_to :category
  
  delegate :name, to: :category, prefix: :category
  def type = party&.party_type&.name
  #def employees = party&.from_relationships.map{|x| x&.to&.detail&.full_name }.uniq
  #def employees = party&.from_relationships.map{|x| [x&.to&.detail&.full_name, x&.to.detail.party.to_relationships.map{|x| [x&.relationship&.name, x&.from&.detail&.official_name]} ]  }.uniq
  def employees = party&.from_relationships.map do |rel| 
    [rel.to.detail_id, rel&.to&.detail&.full_name, rel&.to.detail.party.to_relationships.map do |rel| 
      rel&.relationship&.name
    end 
    ]  
  end.uniq

  def areas = party&.to_relationships.map do |area|
    area&.from&.detail&.name
  end
end
