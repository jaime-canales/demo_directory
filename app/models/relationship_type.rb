class RelationshipType < EntityType
  has_many :relationships, -> { where(type: "Relationship") }, class_name: "EntityRelationship", foreign_key: :relationship_id
  has_one :allowed_relationship
end
