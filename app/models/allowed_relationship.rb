class AllowedRelationship < ApplicationRecord
  belongs_to :party_type
  belongs_to :relationship_type
end
