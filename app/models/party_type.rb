class PartyType < EntityType
  has_many :parties
  has_one :allowed_relationship
end
