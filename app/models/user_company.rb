class UserCompany
  attr_accessor :id, :name, :type, :email, :management, :updated_at

  def initialize(id:, name:, type:, email:, management:, updated_at:)
    @id = id
    @name = name
    @type = type
    @email = email
    @management = management
    @updated_at = updated_at
  end
end
