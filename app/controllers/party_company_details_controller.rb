class PartyCompanyDetailsController < ApplicationController
  before_action :set_party_company_detail, only: %i[ show edit update destroy ]

  # GET /party_company_details or /party_company_details.json
  def index
    companies = PartyCompanyDetail.all.each_with_object([]) do |x, arr|
      management = if x.party&.from_relationships&.first&.relationship&.name.present?
                     "Tiene como #{x.party&.from_relationships&.first.relationship&.name} legal a #{x.party.from_relationships&.first.to&.detail&.slice(:first_name, :last_name)&.values&.join(" ")}"
                   else
                     "No tiene representante legal"
                   end
      arr << UserCompany.new(id: x.id,
                             name: x.common_name,
                             type: x.party.party_type.name,
                             email: x.email_company,
                             management: management,
                             updated_at: x.updated_at)
    end
    users = PartyPersonDetail.all.each_with_object([]){|x, arr| arr << UserCompany.new(id: x.id, name: "#{x.first_name} #{x.last_name}", type: x.party.party_type.name, email: x.email, management: "Es #{x.party&.to_relationships&.first.relationship&.name} de #{x.party&.to_relationships&.first.from&.detail&.official_name}", updated_at: x.updated_at) } 
    @companies = (companies + users).sort_by{|x| x.name }
  end

  # GET /party_company_details/1 or /party_company_details/1.json
  def show
  end

  # GET /party_company_details/new
  def new
    @party_company_detail = PartyCompanyDetail.new
  end

  # GET /party_company_details/1/edit
  def edit
  end

  # POST /party_company_details or /party_company_details.json
  def create
    @party_company_detail = PartyCompanyDetail.new(party_company_detail_params)
    respond_to do |format|
      if @party_company_detail.save
        party_company = Party.create(
          party_type: party_type_company,
          detail: @party_company_detail
        )

        if party_responsible_params.values.all?(&:present?)
          person = PartyPersonDetail.create(party_responsible_params)
          party_person = Party.create(
            party_type: person_company,
            detail: person
          )
          Relationship.create(
            relationship: responsible_company,
            from: party_company,
            to: party_person
          )

          Relationship.create(
            relationship: worker_of,
            from: party_company,
            to: party_person
          )
        end
        format.html { redirect_to party_company_detail_url(@party_company_detail), notice: "Party company detail was successfully created." }
        format.json { render :show, status: :created, location: @party_company_detail }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @party_company_detail.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /party_company_details/1 or /party_company_details/1.json
  def update
    respond_to do |format|
      if @party_company_detail.update(party_company_detail_params)
        format.html { redirect_to party_company_detail_url(@party_company_detail), notice: "Party company detail was successfully updated." }
        format.json { render :show, status: :ok, location: @party_company_detail }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @party_company_detail.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /party_company_details/1 or /party_company_details/1.json
  def destroy
    @party_company_detail.destroy

    respond_to do |format|
      format.html { redirect_to party_company_details_url, notice: "Party company detail was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
  def set_party_company_detail
    @party_company_detail = PartyCompanyDetail.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def party_company_detail_params
    params.require(:party_company_detail).permit(:common_name, :official_name, :taxes_registry_number, :country_code, :company_address, :category_id, :email_company)
  end

  def party_responsible_params
    params.require(:party_company_detail).permit(:first_name, :last_name, :phone, :email, :person_address, :identification_document_number, :identification_document_type)
  end

  def party_type_company
    @party_type_company ||= PartyType.find_by(name: "company")
  end

  def responsible_company
    @responsible_company ||= RelationshipType.find_by(name: "representante")
  end

  def person_company
    @person_company ||= PartyType.find_by(name: "person")
  end

  def worker_of
    @worker_of ||= RelationshipType.find_by_name("trabajador")
  end
end
