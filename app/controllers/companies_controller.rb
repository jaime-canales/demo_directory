class CompaniesController < ApplicationController
  def index
    @companies = PartyCompanyDetail.all
  end

  def show
    @company = PartyCompanyDetail.find_by_id(params[:id])
  end
end
