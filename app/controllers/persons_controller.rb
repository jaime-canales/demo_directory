class PersonsController < ApplicationController
  def index
    @persons = PartyPersonDetail.all
  end

  def show
    @person = PartyPersonDetail.find_by_id(params[:id])
  end
end
