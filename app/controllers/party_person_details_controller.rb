class PartyPersonDetailsController < ApplicationController
  def new
    @party_person_detail = PartyPersonDetail.new
  end

  def edit
  end

  def create
    @party_person_detail = PartyPersonDetail.new(party_person_detail_params.except(:company_id))
    respond_to do |format|
      if @party_person_detail.save
        party_person = Party.create(
          party_type: person_company,
          detail: @party_person_detail
        )
        if params.dig(:party_person_detail, :company_id).present?
          Relationship.create(
            relationship: worker_of,
            from: company.party,
            to: party_person
          )
        end

        format.html { redirect_to party_person_detail_url(@party_person_detail), notice: "Party person detail was successfully created." }
        format.json { render :show, status: :created, location: @party_person_detail }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @party_person_detail.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def party_person_detail_params
    params.require(:party_person_detail).permit(:first_name, :last_name, :phone, :email, :person_address, :identification_document_number, :identification_document_type, :company_id)
  end

  def worker_of
    @worker_of ||= RelationshipType.find_by(name: "trabajador")
  end

  def company
    @company ||= PartyCompanyDetail.find_by_id(params.dig(:party_person_detail, :company_id))
  end

  def person_company
    @person_company ||= PartyType.find_by(name: "person")
  end
end
