class CreatePartyPersonDetails < ActiveRecord::Migration[7.0]
  def change
    create_table :party_person_details do |t|
      t.string :first_name
      t.string :last_name
      t.date :birthdate
      t.string :phone
      t.string :email
      t.string :address
      t.string :identification_document_number
      t.integer :identification_document_type, default: 0

      t.timestamps
    end
  end
end
