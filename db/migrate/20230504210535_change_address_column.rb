class ChangeAddressColumn < ActiveRecord::Migration[7.0]
  def change
    rename_column :party_person_details, :address, :person_address
    rename_column :party_company_details, :address, :company_address
  end
end
