class CreateParties < ActiveRecord::Migration[7.0]
  def change
    create_table :parties do |t|
      t.integer :party_type_id
      t.string :detail_type
      t.integer :detail_id

      t.timestamps
    end
    add_index :parties, :party_type_id
    add_index :parties, :detail_id
  end
end
