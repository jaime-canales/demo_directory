class CreatePartyCompanyDetails < ActiveRecord::Migration[7.0]
  def change
    create_table :party_company_details do |t|
      t.string :common_name
      t.string :official_name
      t.string :taxes_registry_number
      t.string :country_code
      t.string :address
      t.integer :category_id

      t.timestamps
    end
    add_index :party_company_details, :category_id
  end
end
