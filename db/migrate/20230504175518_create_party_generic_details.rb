class CreatePartyGenericDetails < ActiveRecord::Migration[7.0]
  def change
    create_table :party_generic_details do |t|
      t.string :name
      t.jsonb :data, default: '{}'

      t.timestamps
    end
  end
end
