class CreateEntityRelationships < ActiveRecord::Migration[7.0]
  def change
    create_table :entity_relationships do |t|
      t.integer :relationship_id
      t.string :relationship_type
      t.integer :parent_id
      t.integer :child_id
      t.string :type
      t.datetime :effective_from
      t.datetime :effective_to

      t.timestamps
    end
    add_index :entity_relationships, :relationship_id
    add_index :entity_relationships, :parent_id
    add_index :entity_relationships, :child_id
  end
end
