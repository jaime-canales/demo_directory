class CreateAllowedRelationships < ActiveRecord::Migration[7.0]
  def change
    create_table :allowed_relationships do |t|
      t.integer :party_type_id
      t.integer :relationship_type_id

      t.timestamps
    end
    add_index :allowed_relationships, :party_type_id
    add_index :allowed_relationships, :relationship_type_id
  end
end
