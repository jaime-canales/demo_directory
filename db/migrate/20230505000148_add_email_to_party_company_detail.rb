class AddEmailToPartyCompanyDetail < ActiveRecord::Migration[7.0]
  def change
    add_column :party_company_details, :email_company, :string
  end
end
