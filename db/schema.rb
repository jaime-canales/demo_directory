# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2023_05_05_000148) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "allowed_relationships", force: :cascade do |t|
    t.integer "party_type_id"
    t.integer "relationship_type_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["party_type_id"], name: "index_allowed_relationships_on_party_type_id"
    t.index ["relationship_type_id"], name: "index_allowed_relationships_on_relationship_type_id"
  end

  create_table "categories", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "entity_relationships", force: :cascade do |t|
    t.integer "relationship_id"
    t.string "relationship_type"
    t.integer "parent_id"
    t.integer "child_id"
    t.string "type"
    t.datetime "effective_from"
    t.datetime "effective_to"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["child_id"], name: "index_entity_relationships_on_child_id"
    t.index ["parent_id"], name: "index_entity_relationships_on_parent_id"
    t.index ["relationship_id"], name: "index_entity_relationships_on_relationship_id"
  end

  create_table "entity_types", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.string "type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "parties", force: :cascade do |t|
    t.integer "party_type_id"
    t.string "detail_type"
    t.integer "detail_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["detail_id"], name: "index_parties_on_detail_id"
    t.index ["party_type_id"], name: "index_parties_on_party_type_id"
  end

  create_table "party_company_details", force: :cascade do |t|
    t.string "common_name"
    t.string "official_name"
    t.string "taxes_registry_number"
    t.string "country_code"
    t.string "company_address"
    t.integer "category_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "email_company"
    t.index ["category_id"], name: "index_party_company_details_on_category_id"
  end

  create_table "party_generic_details", force: :cascade do |t|
    t.string "name"
    t.jsonb "data", default: "{}"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "party_person_details", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.date "birthdate"
    t.string "phone"
    t.string "email"
    t.string "person_address"
    t.string "identification_document_number"
    t.integer "identification_document_type", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
