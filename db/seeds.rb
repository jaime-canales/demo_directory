# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)
## Actions
RelationshipType.create(name: "representante") # es representante legal de ?
RelationshipType.create(name: "trabajador") # es trabajador de ?
RelationshipType.create(name: "se relaciona con")

## Actors
PartyType.create(name: "company")
PartyType.create(name: "person")
PartyType.create(name: "area")

PartyGenericDetail.create(name: "tecnologia")

Category.create(name: "innmobiliario")
Category.create(name: "licores")
Category.create(name: "tecnologia")
