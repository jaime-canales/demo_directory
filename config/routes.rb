Rails.application.routes.draw do
  resources :party_company_details
  resources :party_person_details
  resources :persons
  resources :companies
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  root "party_company_details#index"
end
